package com.example.incivisme_proyecte;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SharedViewModel extends AndroidViewModel {
    private final Application app;


    public SharedViewModel(@NonNull Application application) {
        super(application);

        this.app = application;
    }


    String usuario_test;
    public void setUser(String userid) {
        System.out.println(userid+ " SET USUARIO TEST");
        usuario_test=userid;
    }
    public String getUser() {
        System.out.println( usuario_test+ " GET USUARIO TEST");
        return usuario_test;
    }


}