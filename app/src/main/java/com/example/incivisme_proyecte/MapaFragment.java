package com.example.incivisme_proyecte;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MapaFragment extends Fragment implements LocationListener {

    private SharedViewModel model;
    private DatabaseReference uid;
    private DatabaseReference base;
    private DatabaseReference quedadas;
    private   DatabaseReference users;
    private LocationManager manager;
    private final int min_time=1000;
    private final int distance=1;
    private double my_longitude;
    private double my_latitude;
    private SupportMapFragment mapFragment;
    public MapaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapa, container, false);
         mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);

         model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        mapFragment.getMapAsync(map -> {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            map.setMyLocationEnabled(true);
            manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

            getLocationUpdates();



            FirebaseAuth auth = FirebaseAuth.getInstance();
            base = FirebaseDatabase.getInstance("https://incivismeproyecto-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
            users = base.child("users");

            uid = users.child(model.getUser());
            quedadas = uid.child("Quedadas");
            quedadas.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Quedada quedada = dataSnapshot.getValue(Quedada.class);

                    QuedadasInfoWindowAdapter customInfoWindow = new QuedadasInfoWindowAdapter(
                            getActivity()
                    );
                    LatLng aux = new LatLng(
                            Double.parseDouble(quedada.getLatitud()),
                            Double.parseDouble(quedada.getLongitud())
                    );
                    if(quedada.getTipo().equals("REUNION")){
                        Marker marker = map.addMarker(new MarkerOptions()
                                .title(quedada.getDireccion())
                                .snippet(quedada.getDetalles())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                                .position(aux));
                        marker.setTag(quedada);
                        map.setInfoWindowAdapter(customInfoWindow);}

                    if(quedada.getTipo().equals("AMIGOS")){
                        Marker marker = map.addMarker(new MarkerOptions()
                                .title(quedada.getDireccion())
                                .snippet(quedada.getDetalles())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                                .position(aux));
                        marker.setTag(quedada);
                        map.setInfoWindowAdapter(customInfoWindow);
                    }

                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        });

            return view;
    }

    private void getLocationUpdates() {
        System.out.println("getLocationUpdates");
        if (manager!=null){
            if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, min_time, distance, this);
                } else if (manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, min_time, distance, this);
                }
            }
            else {
                    ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},101);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==101){
            if(grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED){
                getLocationUpdates();
            }
            else {
                Toast.makeText(getContext(),"Permission Required",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        saveLocation(location);

    }

    private void saveLocation(Location location) {
        my_latitude=location.getLatitude();
        my_longitude=location.getLongitude();
        System.out.println("LOCATION MAPA LATITUDE:" + location.getLatitude());
        System.out.println("LOCATION MAPA LONGITUD:" + location.getLongitude());
        LatLng aux_personal = new LatLng(
                my_latitude,
                my_longitude
        );
        mapFragment.getMapAsync(map -> {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        Marker marker = map.addMarker(new MarkerOptions()
                .title("My Location")
                .snippet("Estic Aqui")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                .position(aux_personal));
            Quedada quedada=null;
        marker.setTag(quedada);
        });

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }
}