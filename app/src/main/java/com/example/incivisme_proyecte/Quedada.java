package com.example.incivisme_proyecte;

import java.io.Serializable;

public class Quedada implements Serializable {
    private String direccion;
    private String longitud;
    private String latitud;
    private String detalles;
    private String nombre;
    private String numero;
    private String correo;
    private String tipo;
    private String id;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Quedada(String direccion, String longitud, String latitud, String detalles, String nombre, String numero, String correo) {
        this.direccion = direccion;
        this.longitud = longitud;
        this.latitud = latitud;
        this.detalles = detalles;
        this.nombre = nombre;
        this.numero = numero;
        this.correo = correo;
    }

    public Quedada() {
    }
}
