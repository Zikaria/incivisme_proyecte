package com.example.incivisme_proyecte;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class QuedadaFragment extends Fragment {

    private QuedadaViewModel mViewModel;
    private SharedViewModel model;
    private Quedada quedada;
    public static QuedadaFragment newInstance() {
        return new QuedadaFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.quedada_item, container, false);
        Intent i = getActivity().getIntent();
        ImageView img = view.findViewById(R.id.item_img);
        TextView txtDireccio = view.findViewById(R.id.item_direccion);
        TextView txtDetalles = view.findViewById(R.id.item_detalles);
        TextView txtNombre = view.findViewById(R.id.item_nombre);
        TextView txtNumero = view.findViewById(R.id.item_numero);
        TextView txtCorreo = view.findViewById(R.id.item_correo);
        Button buttonDelete = view.findViewById(R.id.btn_eliminar);
        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        if (i != null) {
             quedada = (Quedada) i.getSerializableExtra("quedada");

            if (quedada != null) {
                Glide.with(view).load(quedada.getUrl()).into(img);
                txtDireccio.setText(quedada.getDireccion());
                txtDetalles.setText(quedada.getDetalles());
                txtNombre.setText(quedada.getNombre());
                txtNumero.setText(quedada.getNumero());
                txtCorreo.setText(quedada.getCorreo());

                buttonDelete.setOnClickListener(button -> {

                    System.out.println("BOTON DELETE");
                    DatabaseReference  base = FirebaseDatabase.getInstance("https://incivismeproyecto-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
                    DatabaseReference users = base.child("users");
                    DatabaseReference uid = users.child(model.getUser());
                    System.out.println("uid");
                    DatabaseReference quedadas = uid.child("Quedadas");
                   quedadas.child(quedada.getId()).removeValue();
                    Toast.makeText(getContext(), "Quedada Eliminada", Toast.LENGTH_SHORT).show();

                });
            }


        }
        return view;
    }


}