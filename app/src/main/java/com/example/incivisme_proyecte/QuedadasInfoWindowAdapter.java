package com.example.incivisme_proyecte;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class QuedadasInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final Activity activity;

    public QuedadasInfoWindowAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater()
                .inflate(R.layout.infoview, null);
        ImageView ivImagen = view.findViewById(R.id.iv_imagen);
        TextView tvTitulo = view.findViewById(R.id.tvTitulo);
        TextView tvDescripcio = view.findViewById(R.id.tvDescripcio);

        Quedada quedada = (Quedada) marker.getTag();
        if(quedada!=null){
        tvTitulo.setText(quedada.getDireccion());
        tvDescripcio.setText(quedada.getDetalles());
        Glide.with(activity).load(quedada.getUrl()).into(ivImagen);
        }
        else {
            String titulo="You are here";
            String descripcion="Esta usted aqui señor";
            tvTitulo.setText(titulo);
            tvDescripcio.setText(descripcion);
            Glide.with(activity).load(R.drawable.ic_baseline_person_24).into(ivImagen);
        }


        return view;
    }

}