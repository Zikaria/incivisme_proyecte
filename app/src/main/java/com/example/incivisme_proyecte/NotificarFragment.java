package com.example.incivisme_proyecte;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

public class NotificarFragment extends Fragment {
    ProgressBar mLoading;
    private String latitud="0";
    private String longitud="0";
    private TextInputEditText txtDireccio;
    private TextInputEditText txtDetalles;
    private TextInputEditText txtNombre;
    private TextInputEditText txtNumero;
    private TextInputEditText txtCorreo;
    private Button buttonadd;
    private SharedViewModel model;
    String mCurrentPhotoPath;
    private Uri photoURI;
    private ImageView foto;
    static final int REQUEST_TAKE_PHOTO = 1;
    private DatabaseReference uid;
    private DatabaseReference base;
    private DatabaseReference quedadas;
    private   DatabaseReference users;
    private FirebaseStorage storage ;
    private String downloadUrl;


    public NotificarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificar, container, false);

        mLoading = view.findViewById(R.id.loading);

        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);


        txtDireccio = view.findViewById(R.id.txtDireccio);
        txtDetalles = view.findViewById(R.id.txtDetalles);
        buttonadd = view.findViewById(R.id.button_add);
        txtNombre= view.findViewById(R.id.txtNombre);
        txtNumero = view.findViewById(R.id.txtNumero);
        txtCorreo = view.findViewById(R.id.txtCorreo);
        foto = view.findViewById(R.id.foto);
        Button buttonFoto = view.findViewById(R.id.button_foto);

        System.out.println(txtDireccio.getText().toString());

     //   storage = FirebaseStorage.getInstance();
       // storageRef = storage.getReference();
        buttonFoto.setOnClickListener(button -> {
            System.out.println("HACER FOTO");

            dispatchTakePictureIntent();


        });

        buttonadd.setOnClickListener(button -> {

            RadioGroup radioGroup = view.findViewById(R.id.radiogroup);
            int selectedId = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) view.findViewById(selectedId);


            String location=txtDireccio.getText().toString();

            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            try {
                List<Address> addressList = geocoder.getFromLocationName(location,1);

                if(addressList.size()>0){
                    Address address = addressList.get(0);
                    latitud= String.valueOf(address.getLatitude());
                    longitud= String.valueOf(address.getLongitude());
                    System.out.println("LONGITUD: "+longitud);
                    System.out.println("LATITUDE: "+latitud);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }




            Quedada quedada = new Quedada();
            quedada.setDireccion(txtDireccio.getText().toString());
            quedada.setLatitud(latitud);
            quedada.setLongitud(longitud);
            quedada.setDetalles(txtDetalles.getText().toString());
            quedada.setNombre(txtNombre.getText().toString());
            quedada.setNumero(txtNumero.getText().toString());
            quedada.setCorreo(txtCorreo.getText().toString());
            quedada.setUrl(downloadUrl);
            if(radioButton==null){
                quedada.setTipo("REUNION");
            }
            else {
                quedada.setTipo(radioButton.getText().toString());
            }
            FirebaseAuth auth = FirebaseAuth.getInstance();
            base = FirebaseDatabase.getInstance("https://incivismeproyecto-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
            users = base.child("users");
            uid = users.child(model.getUser());
            quedadas = uid.child("Quedadas");


            DatabaseReference reference = quedadas.push();
            quedada.setId(reference.getKey());
            System.out.println(quedada.getId()+" IDDDD");

            reference.setValue(quedada);

            Toast.makeText(getContext(), "Quedada Añadida", Toast.LENGTH_SHORT).show();
        });

        return view;
    }


    private File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    static final int REQUEST_IMAGE_CAPTURE = 1;


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                getContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {


            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(photoURI).into(foto);
                storage = FirebaseStorage.getInstance("gs://incivismeproyecto.appspot.com/");
                StorageReference storageRef = storage.getReference();
                StorageReference imageRef = storageRef.child(mCurrentPhotoPath);
                UploadTask uploadTask = imageRef.putFile(photoURI);
                System.out.println(uploadTask+" uploadTask URL");

                uploadTask.addOnSuccessListener(taskSnapshot -> {

                    Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                    result.addOnSuccessListener(uri -> {
                        downloadUrl = uri.toString();
                        Toast.makeText(getContext(),
                                "Picture ready to upload", Toast.LENGTH_SHORT).show();
                    });
                });
            } else {
                Toast.makeText(getContext(),
                        "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
            }
        }
    }




}