package com.example.incivisme_proyecte;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class LlistarFragment extends Fragment {
    private SharedViewModel mViewModel;
    private DatabaseReference uid;
    private DatabaseReference base;
    private DatabaseReference quedadas;
    private   DatabaseReference users;
    public LlistarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_llistar, container, false);
       mViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        base = FirebaseDatabase.getInstance("https://incivismeproyecto-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
        users = base.child("users");

        uid = users.child(mViewModel.getUser());
        quedadas = uid.child("Quedadas");


        FirebaseListOptions<Quedada> options = new FirebaseListOptions.Builder<Quedada>()
                .setQuery(quedadas, Quedada.class)
                .setLayout(R.layout.lv_quedada_item)
                .setLifecycleOwner(this)
                .build();


        FirebaseListAdapter<Quedada> adapter = new FirebaseListAdapter<Quedada>(options) {
            @Override
            protected void populateView(View v, Quedada model, int position) {
                TextView txtDescripcio = v.findViewById(R.id.txtDetalles);
                TextView txtAdreca = v.findViewById(R.id.txtDireccio);
                ImageView img = v.findViewById(R.id.foto_lista);

                txtDescripcio.setText(model.getDetalles());
                txtAdreca.setText(model.getDireccion());

                    Glide.with(view).load(model.getUrl()).into(img);


            }
        };

        ListView lvIncidencies = view.findViewById(R.id.lvQuedadas);
        lvIncidencies.setAdapter(adapter);

        lvIncidencies.setOnItemClickListener((adapterView, view1, i, l) -> {
            Quedada quedada = (Quedada) adapterView.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), QuedadaActivity.class);
            intent.putExtra("quedada",  quedada);
            startActivity(intent);
        });
        return view;
    }

}